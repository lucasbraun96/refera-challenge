SELECT f.title, count(f.title) as quantity
FROM rental as r
LEFT JOIN inventory as i on i.inventory_id = r.inventory_id
LEFT JOIN film as f on i.film_id = f.film_id 
GROUP BY f.title
ORDER BY quantity desc
LIMIT 2
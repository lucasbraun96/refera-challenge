SELECT CONCAT(first_name, ' ', last_name) as name
FROM actor as a
LEFT JOIN film_actor as fa on a.actor_id = fa.actor_id
LEFT JOIN (SELECT f.film_id, count(f.film_id) as quantity
FROM rental as r
LEFT JOIN inventory as i on i.inventory_id = r.inventory_id
LEFT JOIN film as f on i.film_id = f.film_id 
GROUP BY f.film_id
ORDER BY quantity desc
LIMIT 16) as sq on sq.film_id = fa.film_id
WHERE fa.film_id = sq.film_id 
GROUP BY name
ORDER BY count(CONCAT(first_name, ' ', last_name)) desc
limit 1





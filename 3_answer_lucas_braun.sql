SELECT sub.month, count(sub.month) as quantity
FROM (SELECT MIN(EXTRACT(MONTH FROM r.rental_date)) as month, c.customer_id
FROM customer as c
LEFT JOIN rental as r on c.customer_id = r.customer_id
GROUP BY r.rental_date, c.customer_id
ORDER BY month) as sub
GROUP BY sub.month
LIMIT 3



